package test;

import org.junit.Assert;
import org.junit.Test;

public class EcuacionesTest {
	
	@Test
	public void prueba1() {
		int resultado = Ecuaciones.ecuacionPrueba(3, 2, 3, 1);
		Assert.assertEquals(8, resultado);

	}
	@Test
	public void prueba2() {
		int resultado = Ecuaciones.ecuacionPrueba(3, 2, 3, 0);
		Assert.assertEquals(18, resultado);

	}
	@Test
	public void prueba3() {
		int resultado = Ecuaciones.ecuacionPrueba(1, 1, 1, 1);
		Assert.assertEquals(4, resultado);

	}
}
